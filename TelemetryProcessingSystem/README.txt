Requirements:

	* Read telemetry in from the attached data file in the format specified

		* T, kW, V, I
	* Process the input and output a log in the format specified

		* T, kW, V, I, kW-avg, V-avg, I-avg
		* Averages are a 5 second sliding window average
		* Only output averages when there are no gaps in the data for the last five seconds
	* Detect anomalies in the input data and output a line describing the anomaly

		* Time gaps greater than 1.5 seconds between inputs
		* kW values < 0.0
		* V values outside range of 480 V +/- 5.0 V
		* I values < 0.0


Design:

TelemetryInputReader

	* Reads the input and stores it as a collection of TelemetryData objects

		* Match line with: ([^,\s-:]+)
	* Sends the entire collection of Telemetry Data to the TelemetryProcessor
	* Logs poorly formatted input to a separate log file


TelemetryData

	* A struct struct that stores one frame of telemetry data and the averages to be computed


DateTime

	* A struct containing the year, month, day, hours, minutes, and seconds


TelemetryProcessor 

	* Calculates averages for each frame of telemetry data
	* Detects anomalies
	* Sends TelemetryData to the TelemetryLogger after processing is complete
	* If any of the data in the sliding window have anomalies, do not calculate the average for any value


TelemetryLogger

	* Given TelemetryData, write it to a log file in the format specified
	* Given an anomaly, write it to the log file


TelemetryProcessingSystem

	* Contains the main function and runs the program



Test Plan:

Unit Tests

	* TelemetryInputReaderTests

		* Read one line, ensure TelemetryData is created properly
		* Read one line, ensure that the amount of TelemetryData objects in the collection is accurate
		* Read empty input, ensure that nothing bad happens
		* Read poorly formatted input, log that to a separate log file
	* TelemetryProcessorTests

		* Given a collection of TelemetryData, ensure that averages are calculated properly
		* Given anomalous data, ensure that the anomalies are handled properly
	* TelemetryLoggerTests

		* Given Telemetry Data, ensure the log file is formatted correctly
		* Ensure anomalies are written to the log file in the proper format


I'm sure there more edge cases I could have tested. For example, I know that the DateTime struct that I made is somewhat broken because it was easier to not support telemetry data that spans across multiple months, years, or daylight savings time.

If I had more time I would run performance tests and stress tests to see how the performance scales as the input does, and at what point the input size becomes too large to be viable.


