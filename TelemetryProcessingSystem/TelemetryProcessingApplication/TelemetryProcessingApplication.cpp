// TelemetryProcessingApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>

using namespace TelemetryProcessing;

int main()
{
	std::cout << "Proccessing telemetry data\n";
	std::string telemetryFile = "..\\Input\\telemetry.dat";
	std::string logFile = "..\\Logs\\telemetry.log";

	//Clear the old logFile since TelemetryProcessor will append to the file
	std::ofstream ofs;
	ofs.open(logFile, std::ofstream::out | std::ofstream::trunc);
	ofs.close();

	TelemetryInputReader inputReader(telemetryFile);
	TelemetryProcessor processor;
	std::vector<TelemetryData> data = processor.processTelemetry(inputReader.getData(), logFile);
	std::cout << "Proccessing complete. The results can be found in \\Logs\\telemetry.log\n";
	std::cin.get();
    return 0;
}

