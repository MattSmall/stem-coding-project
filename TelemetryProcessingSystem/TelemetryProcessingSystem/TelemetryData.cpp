#include "stdafx.h"

namespace TelemetryProcessing
{
	//Assumes that the year and month are the same and that all timestamps are in the same timezone
	//I'm using this simplification because most days have the same length, but months and years have different lengths
	//This should fail when the month changes, when the year changes, and when daylight savings time changes
	//I would have preferred to use ctime, but it does not support fractional seconds
	double DateTime::operator-(DateTime time)
	{
		const double SECONDS_PER_MINUTE = 60;
		const double SECONDS_PER_HOUR = SECONDS_PER_MINUTE * 60;
		const double SECONDS_PER_DAY = SECONDS_PER_HOUR * 24;

		if (this->year == time.year && this->month == time.month)
		{
			double seconds1 = this->day*SECONDS_PER_DAY + this->hours*SECONDS_PER_HOUR + this->minutes*SECONDS_PER_MINUTE + this->seconds;
			double seconds2 = time.day*SECONDS_PER_DAY + time.hours*SECONDS_PER_HOUR + time.minutes*SECONDS_PER_MINUTE + time.seconds;
			return seconds1 - seconds2;
		}
		else
		{
			//ERROR
			return 0;
		}
	}
}

