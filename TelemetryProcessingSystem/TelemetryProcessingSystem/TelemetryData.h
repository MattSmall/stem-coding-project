#pragma once

#include <vector>
#include <string>
namespace TelemetryProcessing
{
	struct DateTime
	{
		int year;
		int month;
		int day;
		int hours;
		int minutes;
		double seconds;
		double operator-(DateTime time);
	};

	const std::string TIME_GAP_ANOMALY = "Anomaly - time gap detected";
	const std::string KW_ANOMALY = "Anomaly - kW values < 0.0";
	const std::string V_ANOMALY = "Anomaly - V values outside range of 480 V +/- 5.0 V";
	const std::string I_ANOMALY = "Anomaly - I values < 0.0";
 
	struct TelemetryData
	{
		DateTime time;
		float kilowatts;
		float voltage;
		float current;
		float kWAvg = -1;
		float vAvg = -1;
		float iAvg = -1;
		std::vector<std::string> anomalies;
	};
}