#include "stdafx.h"
#include "TelemetryInputReader.h"
#include <fstream>
#include <regex>

using namespace std;

namespace TelemetryProcessing
{
	TelemetryInputReader::TelemetryInputReader() {}

	TelemetryInputReader::TelemetryInputReader(string filename)
	{
		string logfile = "..\\Logs\\inputlog.log";
		logger.setLogFile(logfile);
		parseInput(filename);
	}

	vector<TelemetryData> TelemetryInputReader::getData()
	{
		return telemetryDataCollection;
	}

	void TelemetryInputReader::parseInput(string filename)
	{
		string line;
		ifstream infile(filename);
		while (getline(infile, line))
		{			
			DateTime time;
			TelemetryData data;
			
			//This is the regex I would like to use, I tried it on regex101.com and it worked,
			//but so far I've only figured out how to get the capturing groups instead of capturing all matches
			//const regex expression("[^,\\s-:]+"); 
			const regex expression("(.+)-(.+)-(.+) (.+):(.+):(.+), (.+), (.+), (.+)");
			smatch matches;
			
			if (regex_search(line, matches, expression))
			{
				time.year = stoi(matches[1]);
				time.month = stoi(matches[2]);
				time.day = stoi(matches[3]);
				time.hours = stoi(matches[4]);
				time.minutes = stoi(matches[5]);
				time.seconds = stod(matches[6]);
				data.time = time;
				data.kilowatts = stof(matches[7]);
				data.voltage = stof(matches[8]);
				data.current = stof(matches[9]);
			}
			else
			{
				logger.log("Inncorretly formatted input: "+line);
			}
			
			telemetryDataCollection.push_back(data);
			
		}
	}
}
