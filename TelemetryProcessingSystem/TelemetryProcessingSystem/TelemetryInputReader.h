#pragma once
#include "stdafx.h"
#include "TelemetryLogger.h"

namespace TelemetryProcessing
{
	class TelemetryInputReader
	{
	public:
		TelemetryInputReader();
		TelemetryInputReader(std::string filename);
		std::vector<TelemetryData> getData();
	private:
		std::vector<TelemetryData> telemetryDataCollection;
		TelemetryLogger logger;
		void parseInput(std::string filename);
	};
}

