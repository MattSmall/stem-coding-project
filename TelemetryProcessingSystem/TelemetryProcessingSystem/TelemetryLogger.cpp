#include "stdafx.h"
#include "TelemetryLogger.h"
#include <fstream>
#include <iomanip>

using namespace std;

namespace TelemetryProcessing
{
	TelemetryLogger::TelemetryLogger() {}

	TelemetryLogger::TelemetryLogger(string filename)
	{
		setLogFile(filename);
	}

	void TelemetryLogger::setLogFile(string filename)
	{
		logFile = filename;
	}

	void TelemetryLogger::log(std::string logStatement)
	{
		ofstream file;
		file.open(logFile, ios_base::app);
		file << logStatement << "\n";
		file.close();
	}

	void TelemetryProcessing::TelemetryLogger::log(TelemetryData data)
	{
		ofstream file;
		file.open(logFile, ios_base::app);
		for(string anomaly : data.anomalies)
		{
			file << anomaly << "\n";
		}

		file << setprecision(3) << fixed;
		file << data.time.year << "-" << data.time.month << "-" << data.time.day << " ";
		file << data.time.hours << ":" << data.time.minutes << ":" << data.time.seconds << ", ";
		file << data.kilowatts << ", " << data.voltage << ", " << data.current << ", ";
		if (data.kWAvg >= 0) 
		{
			file << data.kWAvg;
		}
		file << ", ";
		if (data.vAvg >= 0)
		{
			file << data.vAvg;
		}
		file << ", ";
		if (data.iAvg >= 0)
		{
			file << data.iAvg;
		}
		file << "\n";
	}

}
