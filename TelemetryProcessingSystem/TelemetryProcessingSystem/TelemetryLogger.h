#pragma once

namespace TelemetryProcessing
{
	class TelemetryLogger
	{
	public:
		TelemetryLogger();
		TelemetryLogger(std::string filename);
		void log(std::string logStatement);
		void log(TelemetryData data);
		void setLogFile(std::string filename);
	private:
		std::string logFile;
	};
}

