#include "stdafx.h"
#include "TelemetryProcessor.h"

using namespace std;

namespace TelemetryProcessing
{
	vector<TelemetryData> TelemetryProcessor::processTelemetry(vector<TelemetryData> telemetryDataCollection, string filename)
	{
		vector<TelemetryData> processedData;
		logger.setLogFile(filename);
		deque<TelemetryData> slidingWindow; // A deque is used because I need an iteratable queue
		for (TelemetryData data : telemetryDataCollection)
		{
			detectAnomalies(data);
			//If the queue is empty, add data
			if (slidingWindow.empty())
			{
				slidingWindow.push_back(data);
			}
			else
			{
				// If a time gap is detected, clear and log the queue and push the current data
				TelemetryData previous = slidingWindow.back();
				if (data.time.seconds - previous.time.seconds > MAX_TIME_GAP)
				{
					clearSlidingWindow(slidingWindow, processedData);
					logger.log(TIME_GAP_ANOMALY);
					slidingWindow.push_back(data);
				}
				else
				{
					// If the first element of the queue is past the window from the current data
					// Calculate averages for the back of the queue
					// Pop the queue and log, and push the current data
					if (data.time - slidingWindow.front().time > WINDOW_SIZE)
					{
						slidingWindow.back().kWAvg = calculateKWAvg(slidingWindow);
						slidingWindow.back().vAvg = calculateVAvg(slidingWindow);
						slidingWindow.back().iAvg = calculateIAvg(slidingWindow);
						logger.log(slidingWindow.front());
						processedData.push_back(slidingWindow.front());
						slidingWindow.pop_front();
						slidingWindow.push_back(data);
					}
					//If the window is not yet full, push the data without calculating averages
					else
					{
						slidingWindow.push_back(data);
					}
				}
			}
		}
		clearSlidingWindow(slidingWindow, processedData);
		return processedData;
	}

	//Calculates the averages for the back of the queue if the time difference between back and front as at least one second less than the window size
	//Clears the sliding window and logs all TelemetryData
	void TelemetryProcessor::clearSlidingWindow(deque<TelemetryData> &slidingWindow, vector<TelemetryData> &processedData)
	{
		if (slidingWindow.back().time - slidingWindow.front().time >= WINDOW_SIZE - 1)
		{
			slidingWindow.back().kWAvg = calculateKWAvg(slidingWindow);
			slidingWindow.back().vAvg = calculateVAvg(slidingWindow);
			slidingWindow.back().iAvg = calculateIAvg(slidingWindow);
		}
		while (!slidingWindow.empty())
		{
			TelemetryData front = slidingWindow.front();
			logger.log(front);
			processedData.push_back(slidingWindow.front());
			slidingWindow.pop_front();
		}
	}

	void TelemetryProcessor::detectAnomalies(TelemetryData &data)
	{
		if (data.kilowatts < 0)
		{
			data.anomalies.push_back(KW_ANOMALY);
			data.kWAvg = -1;
		}
		if (data.voltage < 475 || data.voltage > 485)
		{
			data.anomalies.push_back(V_ANOMALY);
			data.vAvg = -1;
		}
		if (data.current < 0)
		{
			data.anomalies.push_back(I_ANOMALY);
			data.iAvg = -1;
		}
	}

	//Calculates the average kW over the slidingWindow
	//Returns -1 if any data in the window has an anomaly
	float TelemetryProcessor::calculateKWAvg(deque<TelemetryData> slidingWindow)
	{
		float sum = 0;
		for (TelemetryData data : slidingWindow)
		{
			if (!data.anomalies.empty()) 
			{
				return -1;
			}
			else
			{
				sum += data.kilowatts;
			}
		}
		float avg = sum / slidingWindow.size();
		return avg;
	}

	//Calculates the average V over the slidingWindow
	//Returns -1 if any data in the window has an anomaly
	float TelemetryProcessor::calculateVAvg(deque<TelemetryData> slidingWindow)
	{
		float sum = 0;
		for (TelemetryData data : slidingWindow)
		{
			if (!data.anomalies.empty())
			{
				return -1;
			}
			sum += data.voltage;
		}
		float avg = sum / slidingWindow.size();
		return avg;
	}

	//Calculates the average I over the slidingWindow
	//Returns -1 if any data in the window has an anomaly
	float TelemetryProcessor::calculateIAvg(deque<TelemetryData> slidingWindow)
	{
		float sum = 0;
		for (TelemetryData data : slidingWindow)
		{
			if (!data.anomalies.empty())
			{
				return -1;
			}
			sum += data.current;
		}
		float avg = sum / slidingWindow.size();
		return avg;
	}


}
