#pragma once
#include "TelemetryLogger.h"
#include <deque>

namespace TelemetryProcessing
{
	class TelemetryProcessor
	{
	public:
		std::vector<TelemetryData> processTelemetry(std::vector<TelemetryData> telemetryDataCollection, std::string logfile);
	private:
		TelemetryLogger logger;
		const int WINDOW_SIZE = 5;
		const float MAX_TIME_GAP = 1.5;
		void clearSlidingWindow(std::deque<TelemetryData> &slidingWindow, std::vector<TelemetryData> &processedData);
		void detectAnomalies(TelemetryData &data);
		float calculateKWAvg(std::deque<TelemetryData> slidingWindow);
		float calculateVAvg(std::deque<TelemetryData> slidingWindow);
		float calculateIAvg(std::deque<TelemetryData> slidingWindow);
	};
}

