// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

// Headers for CppUnitTest
#include "CppUnitTest.h"
#include "../TelemetryProcessingSystem/TelemetryData.h"
#include "../TelemetryProcessingSystem/TelemetryInputReader.h"
#include "../TelemetryProcessingSystem/TelemetryLogger.h"
#include "../TelemetryProcessingSystem/TelemetryProcessor.h"
#include <string>
#include <vector>
