#include "stdafx.h"
#include "CppUnitTest.h"
#include <fstream>
#include <string.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace TelemetryProcessing;

namespace TelemetryProcessingTests
{		
	string oneLine = "..\\Input\\telemetry_one_line.dat";
	string fiveLines = "..\\Input\\telemetry_five_lines.dat";
	string timeGap = "..\\Input\\telemetry_time_gap.dat";
	string anomalies = "..\\Input\\telemetry_anomalies.dat";
	string allData = "..\\Input\\telemetry.dat";
	string testLogs = "..\\Logs\\testlogs.log";
	string telemetryLogs = "..\\Logs\\telemetry.log";
	
	DateTime getTestDate()
	{
		DateTime time;
		time.year = 2018;
		time.month = 1;
		time.day = 8;
		time.hours = 14;
		time.minutes = 54;
		time.seconds = 42.63;
		return time;
	}

	TelemetryData getTestData()
	{
		TelemetryData data;
		data.time = getTestDate();
		data.kilowatts = 441.781;
		data.voltage = 477.470;
		data.current = 925.254;
		return data;
	}

	void clearLogFile(string filename)
	{
		ofstream ofs;
		ofs.open(filename, ofstream::out | ofstream::trunc);
		ofs.close();
	}
	TEST_CLASS(DateTimeTests)
	{
	public:
		TEST_METHOD(DateSubtraction)
		{
			DateTime time1 = getTestDate();

			//Changing this to a round number to avoid floating point comparisions
			time1.seconds = 42;

			DateTime time2;
			time2.year = 2018;
			time2.month = 1;
			time2.day = 8;
			time2.hours = 14;
			time2.minutes = 54;
			time2.seconds = 47;

			double expected = time2.seconds - time1.seconds;
			Assert::AreEqual(expected, time2 - time1);

			time1.seconds = 42.5;
			expected = time2.seconds - time1.seconds;
			Assert::AreEqual(expected, time2 - time1);

			time1.minutes = 53;
			time1.seconds = 47;
			expected = 60;
			Assert::AreEqual(expected, time2 - time1);
		}
	};

	TEST_CLASS(TelemetryInputReaderTests)
	{
	public:

		TEST_METHOD(ReadOneLine)
		{
			TelemetryInputReader inputReader(oneLine);
			vector<TelemetryData> data = inputReader.getData();

			Assert::AreEqual(data.size(), size_t(1));
			Assert::AreEqual(data[0].time.year, 2018);
			Assert::AreEqual(data[0].time.month, 1);
			Assert::AreEqual(data[0].time.day, 8);
			Assert::AreEqual(data[0].kilowatts, (float) 441.781);
			Assert::AreEqual(data[0].voltage, (float) 477.47);
			Assert::AreEqual(data[0].current, (float) 925.254);
		}

		TEST_METHOD(ReadData)
		{
			TelemetryInputReader inputReader(allData);
			vector<TelemetryData> data = inputReader.getData();
			Assert::AreEqual(data.size(), size_t(97));
		}

	};

	TEST_CLASS(TelemetryLoggerTests)
	{
	public:

		TEST_METHOD(LogOneLine)
		{
			clearLogFile(testLogs);
			TelemetryLogger logger(testLogs);
			string logStatement = "Testing";
			logger.log(logStatement);

			string line;
			ifstream infile(testLogs);
			while (getline(infile, line))
			{
				Assert::AreEqual(logStatement, line);
			}	

		}

		TEST_METHOD(LogManyLines)
		{
			clearLogFile(testLogs);

			TelemetryLogger logger(testLogs);
			string test = "Test";
			const int NUM_LINES = 100;
			for (int i = 0; i < NUM_LINES; i++)
			{
				string logStatement = test + to_string(i);
				logger.log(logStatement);
			}

			string line;
			ifstream infile(testLogs);
			for (int i=0; getline(infile, line); i++)
			{
				Assert::AreEqual(line, test + to_string(i));
			}
		}

		TEST_METHOD(LogTelemetryData)
		{
			clearLogFile(testLogs);

			TelemetryData data = getTestData();
			data.kWAvg = 441.781;
			data.vAvg = 477.470;
			data.iAvg = 925.254;

			TelemetryLogger logger(testLogs);
			logger.log(data);
			string line;
			ifstream infile(testLogs);
			getline(infile, line);
			string expected = "2018-1-8 14:54:42.630, 441.781, 477.470, 925.254, 441.781, 477.470, 925.254";
			Assert::AreEqual(expected, line);
		}

		TEST_METHOD(LogIncompleteTelemetryData)
		{
			clearLogFile(testLogs);

			TelemetryData data = getTestData();

			TelemetryLogger logger(testLogs);
			logger.log(data);
			string line;
			ifstream infile(testLogs);
			getline(infile, line);
			string expected = "2018-1-8 14:54:42.630, 441.781, 477.470, 925.254, , , ";
			Assert::AreEqual(expected, line);
		}
	};

	TEST_CLASS(TelemetryProcessorTests)
	{
	public:
		
		TEST_METHOD(ProcessOneLine)
		{
			TelemetryInputReader inputReader(oneLine);
			TelemetryProcessor processor;
			vector<TelemetryData> data = processor.processTelemetry(inputReader.getData(), testLogs);
			Assert::AreEqual(size_t(1), data.size());
			Assert::AreEqual((float)-1, data[0].kWAvg);
			Assert::AreEqual((float)-1, data[0].vAvg);
			Assert::AreEqual((float)-1, data[0].iAvg);
		}

		TEST_METHOD(ProcessFiveLines)
		{
			TelemetryInputReader inputReader(fiveLines);
			TelemetryProcessor processor;
			vector<TelemetryData> data = processor.processTelemetry(inputReader.getData(), testLogs);
			Assert::AreEqual(size_t(5), data.size());
			//This is how I'm testing equality since float comparisons are tricky
			//It works because we only care about precision up to the 3rd decimal
			Assert::IsTrue(403.544 - data.back().kWAvg < 0.001);
			Assert::IsTrue(479.006 - data.back().vAvg < 0.001);
			Assert::IsTrue(841.897 - data.back().iAvg < 0.001);
		}

		TEST_METHOD(ProcessTimeGap)
		{
			clearLogFile(testLogs);
			TelemetryInputReader inputReader(timeGap);
			TelemetryProcessor processor;
			vector<TelemetryData> data = processor.processTelemetry(inputReader.getData(), testLogs);
			string line;
			ifstream infile(testLogs);
			for (int i = 0; i < 3; i++)
			{
				getline(infile, line);
			}
			Assert::AreEqual(TIME_GAP_ANOMALY, line);

			Assert::IsTrue(data[2].kWAvg < 0);
			Assert::IsTrue(data[2].vAvg < 0);
			Assert::IsTrue(data[2].iAvg < 0);
		}

		TEST_METHOD(ProcessAnomalies)
		{
			clearLogFile(testLogs);
			TelemetryInputReader inputReader(anomalies);
			TelemetryProcessor processor;
			vector<TelemetryData> data = processor.processTelemetry(inputReader.getData(), testLogs);
			string line;
			ifstream infile(testLogs);
			for (int i = 0; i < 5; i++)
			{
				getline(infile, line);
			}
			Assert::AreEqual(KW_ANOMALY, line);
			getline(infile, line);
			Assert::AreEqual(V_ANOMALY, line);
			getline(infile, line);
			Assert::AreEqual(I_ANOMALY, line);

			Assert::IsTrue(data[4].kWAvg < 0);
			Assert::IsTrue(data[4].vAvg < 0);
			Assert::IsTrue(data[4].iAvg < 0);
			Assert::IsTrue(data[5].kWAvg < 0);
			Assert::IsTrue(data[5].vAvg < 0);
			Assert::IsTrue(data[5].iAvg < 0);
		}

		TEST_METHOD(ProcessAllLines)
		{
			TelemetryInputReader inputReader(allData);
			TelemetryProcessor processor;
			processor.processTelemetry(inputReader.getData(), telemetryLogs);
		}
	};
}